# images_and_animations

This is an applications which allows the user to appreciate some of the most beautiful landscapes
in Andalucia and also a library to get some images of puppies from a custom server.

- Uses Android animations and Glide to show pretty cool images.
- Uses Kotlin coroutines to deliver task asynchronously.
- Uses OkHttp3 to download the files.
